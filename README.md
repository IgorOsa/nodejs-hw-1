# RD LAB FE NODEJS HOMEWORK №1

Use NodeJS to implement web-server which hosts and serves files.

## Requirements

- [x] Use standard http module or express framework to implement simple webserver;
- [x] Use fs module to create/modify/read files in file system;
- [x] Write every request info to logs;
- [x] Application should support log, txt, json, yaml, xml, js file extensions ( consider filename may contain '.' symbol);
- [x] App can be launched with 'npm install' and 'npm start' commands just after git pull;
- [x] Application should work at port 8080.

## Acceptance criteria

- [x] Server saves file on createFile request and responds with 200 status, use ‘filename' and ‘content’ body params to transfer file data.
- [x] Server returns list of uploaded files on getFiles request
- [x] Server returns file content on getFile request, use 'filename' url parameter to determine what file you want to retrieve.
- [x] In case there are no file with provided name found, return 400 status.
- [x] Gitlab repo link and project id are saved in Google form

## Optional criteria

- [x] Server handles errors and validates input params for all requests.
- [x] Password protection for files, with additional(optional) parameter in createFile request (password field in in request body), and additional(optional) parameter 'password' in getFile request in url query params (to /api/files/{filename}?password={password}). Server handle invalid and empty password for protected files.
- [x] Ability to modify files content (PUT request to /api/files);
- [x] Ability to delete files (DELETE request to /api/files/{filename})
- [ ] Describe additional endpoints in openapi.yaml
