import express from 'express';
import swaggerUI from 'swagger-ui-express';
import {PORT, swaggerDocument} from './config';

import filesRouter from './routes/files.router';
import logger from './common/logger';

const app = express();

app.use(express.json());

app.use('/doc', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

const greeting = {message: 'Welcome to HW#1 server!'};

app.use(logger.logToConsole);
// app.use(logger.logToFile);

app.get('/', (req, res, next) => {
  if (req.originalUrl === '/') {
    res.send(greeting);
    return;
  }
  next();
});

app.use('/api/files', filesRouter);

app.use((err, req, res, next) => {
  if (err) {
    console.error(err.stack);
    res.status(500).send('Server error');
  }
  next();
});

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
