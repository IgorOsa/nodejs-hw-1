import {existsSync} from 'fs';
import {readFile} from 'fs/promises';
import path from 'path';
import {UPLOADS_PATH} from '../config';
import {checkHashedPassword} from './hashHelpers';

const authFile = () => async (req, res, next) => {
  try {
    const {filename} = req?.params;
    const filePath = path.join(UPLOADS_PATH, `${filename}.pwd`);
    const isPassworded = existsSync(filePath);
    res.locals.unlocked = true;

    if (isPassworded) {
      res.locals.unlocked = false;
      const {password} = req?.query;

      if (password !== undefined && password !== '') {
        const hashedPassword = await readFile(filePath, 'utf-8');
        const comparisonRes = await checkHashedPassword(
            password,
            hashedPassword,
        );

        if (!comparisonRes) {
          res.locals.error = true;
          throw new Error('Wrong filename/password combination');
        }
        res.locals.unlocked = true;
      }
    }
  } catch (err) {
    res.status(403).send({message: err.message});
  }
  return next();
};

export default authFile;
