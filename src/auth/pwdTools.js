import {writeFile} from 'fs/promises';

export const savePasswordToFile = async (path, password) => {
  await writeFile(path, password);
};

export default {savePasswordToFile};
