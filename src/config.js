import path, {dirname} from 'path';
import {fileURLToPath} from 'url';
import YAML from 'yamljs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const swaggerDocument = YAML.load(
    path.join(__dirname, './../doc/openapi.yaml'),
);

export const PORT = 8080;
export const UPLOADS_PATH = path.join(__dirname, '../files');
export const LOGS_PATH = path.join(__dirname, '../logs');
export const DEFAULT_SALT_ROUNDS = 10;

const config = {
  PORT,
  UPLOADS_PATH,
  DEFAULT_SALT_ROUNDS,
};

export default config;
