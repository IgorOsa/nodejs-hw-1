import express from 'express';
import {readdir, readFile, stat, unlink, writeFile} from 'fs/promises';
import path from 'path';
import {existsSync} from 'fs';
import {UPLOADS_PATH} from '../config';
import validator from '../common/validator';
import {hashPassword} from '../auth/hashHelpers';
import {savePasswordToFile} from '../auth/pwdTools';
import authFile from '../auth/authMiddleware';
import getFileExtension from '../utils/getExtension';

const router = new express.Router();

router.get('/', async (req, res) => {
  try {
    const files = (await readdir(UPLOADS_PATH)).filter(
        (el) => el !== '.gitkeep' && getFileExtension(el) !== 'pwd',
    );
    res.status(200).send({message: 'Success', files});
  } catch (err) {
    const {message} = err;
    res.status(400).send({message});
  }
});

router.get(
    '/:filename',
    validator.fileExtensionValidator('params'),
    validator.passwordValidator('query'),
    authFile(),
    async (req, res) => {
      const {unlocked, error} = res?.locals;

      if (error) {
        return;
      }

      if (!unlocked) {
        res.status(401).send({message: 'Unauthorized'});
        return;
      }

      const {filename} = req.params;
      const filePath = path.join(UPLOADS_PATH, filename);
      const extension = getFileExtension(filename);

      try {
        const content = await readFile(filePath, 'utf-8');
        const uploadedDate = (await stat(filePath)).birthtime;
        res.status(200).send({
          message: 'Success',
          filename,
          content,
          extension,
          uploadedDate,
        });
      } catch (err) {
        res
            .status(400)
            .send({message: `No file with '${filename}' filename found`});
      }
    },
);

router.post(
    '/',
    validator.bodyValidator('filename'),
    validator.fileExtensionValidator(),
    validator.bodyValidator('content'),
    validator.passwordValidator(),
    async (req, res) => {
      const {filename, content, password} = req.body;
      const filePath = path.join(UPLOADS_PATH, filename);

      try {
        const isExists = existsSync(filePath);

        if (isExists) {
          throw new Error(`File already exists!`);
        }

        await writeFile(filePath, content);
        if (password !== undefined) {
          const hashedPassword = await hashPassword(password);
          await savePasswordToFile(
              path.join(UPLOADS_PATH, `${filename}.pwd`),
              hashedPassword,
          );
        }
        res.status(200).send({message: 'File created successfully!'});
      } catch (err) {
        const {message} = err;
        res.status(400).send({message});
      }
    },
);

router.put(
    '/',
    validator.bodyValidator('filename'),
    validator.fileExtensionValidator(),
    validator.bodyValidator('content'),
    async (req, res) => {
      const {filename, content} = req.body;
      const filePath = path.join(UPLOADS_PATH, filename);

      try {
        const isExists = existsSync(filePath);

        if (!isExists) {
          throw new Error(`File doesn't exists!`);
        }

        await writeFile(filePath, content);
        res
            .status(200)
            .send({message: `File ${filename} updated successfully!`});
      } catch (err) {
        const {message} = err;
        res.status(400).send({message});
      }
    },
);

router.delete(
    '/:filename',
    validator.fileExtensionValidator('params'),
    async (req, res) => {
      const {filename} = req.params;
      const filePath = path.join(UPLOADS_PATH, filename);

      try {
        const isExists = existsSync(filePath);

        if (!isExists) {
          throw new Error(`File does not exist or has already been deleted!`);
        }

        await unlink(filePath);

        res
            .status(200)
            .send({message: `File ${filename} deleted successfully!`});
      } catch (err) {
        const {message} = err;
        res.status(400).send({message});
      }
    },
);

export default router;
