// eslint-disable-next-line require-jsdoc
export default function getFileExtension(filename) {
  const extArr = filename.split('.');
  return extArr[extArr?.length - 1];
}
