import getFileExtension from '../utils/getExtension';

const fileExtensionValidator =
  (property = 'body') =>
    (req, res, next) => {
      const VALID_FILE_EXTENSIONS = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
      const {filename} = req[property];
      const ext = getFileExtension(filename);

      if (VALID_FILE_EXTENSIONS.includes(ext)) {
        return next();
      }
      return res.status(400).json({
        message: `Wrong file extension! 
Only log, txt, json, yaml, xml, js file extensions are supported`,
      });
    };

const bodyValidator =
  (fieldName = 'content') =>
    (req, res, next) => {
      const fieldValue = req?.body[fieldName];

      if (fieldValue === undefined || fieldValue === '') {
        return res.status(400).json({
          message: `Please specify '${fieldName}' parameter`,
        });
      }
      return next();
    };

const passwordValidator =
  (param = 'body') =>
    async (req, res, next) => {
      const {password} = req[param];

      if (password === '') {
        return res.status(400).json({
          message: `Password cannot be empty`,
        });
      }
      return next();
    };

export default {bodyValidator, fileExtensionValidator, passwordValidator};
