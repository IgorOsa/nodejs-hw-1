import morgan from 'morgan';
import {createWriteStream} from 'fs';
import path from 'path';
import {LOGS_PATH} from '../config';

const logAccessFile = createWriteStream(path.join(LOGS_PATH, 'access.log'), {
  flags: 'a',
  encoding: 'utf8',
});

const logToConsole = morgan('dev');
const logToFile = morgan('combined', {stream: logAccessFile});

export default {logToConsole, logToFile};
